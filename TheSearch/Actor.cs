﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    public delegate void DeathHandler(Actor actor);

    public abstract class Actor : MovableObject
    {
        private bool isProtected;
        private int maxHealth;
        private int health;
        private SolidBrush healthBrush;
        private Pen borderPen;

        public event DeathHandler Died;

        public int Health
        {
            get { return health; }
            set { health = value; }
        }

        protected bool IsProtected { get { return isProtected; } }
        protected int MaxHealth { get { return maxHealth; } }
        protected Pen BorderPen { get { return borderPen; } }

        protected Actor(RectangleF bounds, Bitmap image, int speed, int health)
            : base(bounds, image, speed)
        {
            isProtected = false;
            this.health = health;
            this.maxHealth = health;
            healthBrush = new SolidBrush(Color.Red);
            borderPen = new Pen(Color.Black, 3);
        }

        public virtual void Hit(int damage)
        {
            if (!isProtected)
            {
                health -= damage;
                if (health <= 0)
                {
                    DeathHandler handler = Died;
                    if (handler != null)
                        Died(this);
                }
                else
                    StartProtection();
            }
        }

        private void StartProtection()
        {
            System.Threading.Thread protectionThread = new System.Threading.Thread(EndProtection);
            protectionThread.Start();
            while(!protectionThread.IsAlive);
        }

        private void EndProtection()
        {
            try  {
                isProtected = true;
                System.Threading.Thread.Sleep(750);
                isProtected = false;
            }
            catch (Exception e)
            {
                Console.WriteLine("ProtectionThread");
                Console.WriteLine(e.StackTrace);
            }
        }
        
        public override void Draw(Graphics grph)
        {
            base.Draw(grph);

            grph.TranslateTransform(Bounds.Location.X, Bounds.Location.Y);
            Point healthPoint = new Point(0, -35);
            int healthHeight = 10;
            grph.FillRectangle(healthBrush, new RectangleF(healthPoint, new SizeF(Bounds.Width * (float)health/(float)maxHealth, healthHeight)));
            grph.DrawRectangle(borderPen, new Rectangle(healthPoint, new Size((int)Bounds.Width, healthHeight)));
            grph.ResetTransform();
        }
    }
}
