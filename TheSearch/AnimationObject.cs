﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;
using System.Drawing.Imaging;

namespace TheSearch
{
    public abstract class AnimationObject : GameObject
    {
        private bool animate;
        private int activeFrame;
        private float animationRotation;
        private PointF actorCenter;
        private RectangleF animationBounds;
        private Bitmap animation;
        private Timer timer;

        public PointF ActorCenter
        {
            get { return actorCenter; }
            set { actorCenter = value; }
        }

        protected AnimationObject(RectangleF bounds, Bitmap image, int animationDuration)
            : base(bounds, image)
        {
            activeFrame = 0;
            animationRotation = 0.0f;

            timer = new Timer(animationDuration);
            timer.Elapsed += new ElapsedEventHandler(Animate);
        }

        public virtual void StartAnimation(float rotation, PointF actorCenter, Bitmap animation, RectangleF animationBounds)
        {
            if (!animate) 
            {
                this.animation = animation;
                animate = true;
                this.actorCenter = actorCenter;
                this.animationBounds = animationBounds;
                animationRotation = rotation;
                timer.Start();
            }
        }

        private void Animate(object sender, ElapsedEventArgs args)
        {
            try {
                lock (animation)
                {
                    FrameDimension dimension = new FrameDimension(animation.FrameDimensionsList[0]);
                    int frames = animation.GetFrameCount(dimension);
            
                    if (activeFrame < frames)
                    {
                        animation.SelectActiveFrame(dimension, activeFrame);
                        activeFrame++;
                    }
                    else
                    {
                        activeFrame = 0;
                        animate = false;
                        timer.Stop();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Animate");
                Console.WriteLine(e.StackTrace);
            }
        }

        public override void Draw(Graphics grph)
        {
            base.Draw(grph);
            if (animate)
            {
                grph.TranslateTransform(actorCenter.X, actorCenter.Y);
                grph.RotateTransform(animationRotation);
                lock (animation)
                    grph.DrawImage(animation, animationBounds);
                grph.ResetTransform();
            }
        }
    }
}
