﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class Bolt : Projectile
    {
        private bool isHarmless;

        public Bolt(RectangleF bounds, Bitmap image, int damage, bool friendly)
            : base(bounds, image, damage, 20, friendly)
        {
            isHarmless = false;
        }

        protected override void Collide(CollisionObject collider)
        {
 	        if (!isHarmless && collider.GetType().IsSubclassOf(typeof(Actor)))
            {
                if ((Friendly && collider.GetType().IsSubclassOf(typeof(Enemy))) || 
                    (!Friendly && collider.GetType().Equals(typeof(Player))))
                {
                    ((Actor)collider).Hit(Damage);
                    MakeHarmless();
                }
            }
            else if (isHarmless && collider.GetType().Equals(typeof(Player)) && Friendly)
            {
                Player player = (Player)collider;
                CollisionObject.RemoveCollider(this);
                Bow bow = (Bow)player.Weapon;
                bow.AddBolt(this);
            }
            else if (collider.GetType().Equals(typeof(Wall)))
                MakeHarmless();
        }

        private void MakeHarmless()
        {
            Damage = 0;
            Speed = 0;
            RemoveDir(MoveDir.Right | MoveDir.Left);
            RemoveDir(MoveDir.Up | MoveDir.Down);
            isHarmless = true;
        }
    }

            
}
