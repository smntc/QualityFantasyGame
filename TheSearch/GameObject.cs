﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class GameObject
    {
        protected RectangleF bounds;

        protected float Rotation { get; set; }
        protected Bitmap Image { get; set; }
        public RectangleF Bounds { get { return bounds; } }

        protected GameObject(RectangleF bounds, Bitmap image) 
        {
            this.bounds = bounds;
            Image = image;
            Rotation = 0.0f;
        }

        public PointF CalcCenter()
        {
            return new PointF(Bounds.X + Bounds.Width / 2.0f, Bounds.Y + Bounds.Height / 2.0f);
        }

        public virtual void Draw(Graphics grph)
        {
            if (Image != null)
            {
                float offsetX = Bounds.X + Bounds.Width / 2.0f;
                float offsetY = Bounds.Y + Bounds.Height / 2.0f;
                grph.TranslateTransform(offsetX, offsetY);
                grph.RotateTransform(Rotation);
                grph.DrawImage(Image, new RectangleF(new PointF(-Bounds.Width / 2.0f, -Bounds.Height / 2.0f), Bounds.Size));
                grph.ResetTransform();
            }
        }
    }
}
