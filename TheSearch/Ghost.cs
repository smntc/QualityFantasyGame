﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Timers;

namespace TheSearch
{
    class Ghost : Enemy
    {
        public Ghost(RectangleF bounds, Bitmap image, PointF playerPos)
            : base(bounds, image, playerPos, 3, 75, 10)
        {
            Timer.Interval = 500;
        }

        protected override void Act(object sender, ElapsedEventArgs args) 
        {
            try {
                PointF pos = Bounds.Location;

                if (pos.X - PlayerPos.X >= 0)
                    Move(MoveDir.Left);
                else if (pos.X - PlayerPos.X < 0)
                    Move(MoveDir.Right);

                if (pos.Y - PlayerPos.Y >= 0)
                    Move(MoveDir.Up);
                else if (pos.Y - PlayerPos.Y < 0)
                    Move(MoveDir.Down);

                Timer.Interval = Rand.Next(200, 750);
            }
            catch (Exception e)
            {
                Console.WriteLine("GhostAct");
                Console.WriteLine(e.StackTrace);
            }
        }

        public override void Draw(Graphics grph)
        {
            float tmp = Rotation;
            Rotation = 0;
            base.Draw(grph);
            Rotation = tmp;
        }
    }
}
