﻿namespace TheSearch
{
    partial class MainWin
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.classLabel = new System.Windows.Forms.Label();
            this.knightButton = new System.Windows.Forms.Button();
            this.archerButton = new System.Windows.Forms.Button();
            this.paladinButton = new System.Windows.Forms.Button();
            this.knightBox = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.archerBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.paladinBox = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.knightBox.SuspendLayout();
            this.archerBox.SuspendLayout();
            this.paladinBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // classLabel
            // 
            this.classLabel.AutoSize = true;
            this.classLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.classLabel.Location = new System.Drawing.Point(482, 179);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(99, 29);
            this.classLabel.TabIndex = 0;
            this.classLabel.Text = "Klasse:";
            this.classLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // knightButton
            // 
            this.knightButton.Location = new System.Drawing.Point(121, 240);
            this.knightButton.Name = "knightButton";
            this.knightButton.Size = new System.Drawing.Size(90, 51);
            this.knightButton.TabIndex = 1;
            this.knightButton.Text = "Ritter";
            this.knightButton.UseVisualStyleBackColor = true;
            this.knightButton.Click += new System.EventHandler(this.ClassButtonClicked);
            // 
            // archerButton
            // 
            this.archerButton.Location = new System.Drawing.Point(458, 240);
            this.archerButton.Name = "archerButton";
            this.archerButton.Size = new System.Drawing.Size(135, 51);
            this.archerButton.TabIndex = 2;
            this.archerButton.Text = "Bogenschütze";
            this.archerButton.UseVisualStyleBackColor = true;
            this.archerButton.Click += new System.EventHandler(this.ClassButtonClicked);
            // 
            // paladinButton
            // 
            this.paladinButton.Location = new System.Drawing.Point(925, 240);
            this.paladinButton.Name = "paladinButton";
            this.paladinButton.Size = new System.Drawing.Size(112, 51);
            this.paladinButton.TabIndex = 3;
            this.paladinButton.Text = "Zauberer";
            this.paladinButton.UseVisualStyleBackColor = true;
            this.paladinButton.Click += new System.EventHandler(this.ClassButtonClicked);
            // 
            // knightBox
            // 
            this.knightBox.Controls.Add(this.label13);
            this.knightBox.Controls.Add(this.label3);
            this.knightBox.Controls.Add(this.label2);
            this.knightBox.Controls.Add(this.label4);
            this.knightBox.Controls.Add(this.label1);
            this.knightBox.Location = new System.Drawing.Point(58, 345);
            this.knightBox.Name = "knightBox";
            this.knightBox.Size = new System.Drawing.Size(222, 168);
            this.knightBox.TabIndex = 4;
            this.knightBox.TabStop = false;
            this.knightBox.Text = "Ritter";
            this.knightBox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(171, 20);
            this.label13.TabIndex = 3;
            this.label13.Text = "Zauber: Unverwundbar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "- Geringe Reichweite";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "- Langsam";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "- Hoher Schaden";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "- Hohe Gesundheit";
            // 
            // archerBox
            // 
            this.archerBox.Controls.Add(this.label14);
            this.archerBox.Controls.Add(this.label8);
            this.archerBox.Controls.Add(this.label7);
            this.archerBox.Controls.Add(this.label6);
            this.archerBox.Controls.Add(this.label5);
            this.archerBox.Location = new System.Drawing.Point(408, 345);
            this.archerBox.Name = "archerBox";
            this.archerBox.Size = new System.Drawing.Size(245, 168);
            this.archerBox.TabIndex = 5;
            this.archerBox.TabStop = false;
            this.archerBox.Text = "Bogenschütze";
            this.archerBox.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 20);
            this.label5.TabIndex = 1;
            this.label5.Text = "- Hohe Ausdauer";
            // 
            // paladinBox
            // 
            this.paladinBox.Controls.Add(this.label15);
            this.paladinBox.Controls.Add(this.label12);
            this.paladinBox.Controls.Add(this.label11);
            this.paladinBox.Controls.Add(this.label10);
            this.paladinBox.Controls.Add(this.label9);
            this.paladinBox.Location = new System.Drawing.Point(853, 345);
            this.paladinBox.Name = "paladinBox";
            this.paladinBox.Size = new System.Drawing.Size(233, 168);
            this.paladinBox.TabIndex = 6;
            this.paladinBox.TabStop = false;
            this.paladinBox.Text = "Zauberer";
            this.paladinBox.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 123);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 20);
            this.label15.TabIndex = 4;
            this.label15.Text = "Zauber: Feuerball";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 20);
            this.label12.TabIndex = 3;
            this.label12.Text = "- Niedriger Schaden";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(152, 20);
            this.label11.TabIndex = 2;
            this.label11.Text = "- Mittlere Reichweite";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(190, 20);
            this.label10.TabIndex = 1;
            this.label10.Text = "- Mittlere Geschwindigkeit";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "- Hohe Mana";
            // 
            // okButton
            // 
            this.okButton.Enabled = false;
            this.okButton.Location = new System.Drawing.Point(470, 644);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(123, 65);
            this.okButton.TabIndex = 7;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButtonClicked);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 20);
            this.label6.TabIndex = 2;
            this.label6.Text = "- Schnell";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "- Hohe Reichweite";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 20);
            this.label8.TabIndex = 4;
            this.label8.Text = "- Mittlerer Schaden";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 123);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(198, 20);
            this.label14.TabIndex = 5;
            this.label14.Text = "Zauber: Erhöhter Schaden";
            // 
            // MainWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 745);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.paladinBox);
            this.Controls.Add(this.archerBox);
            this.Controls.Add(this.knightBox);
            this.Controls.Add(this.paladinButton);
            this.Controls.Add(this.archerButton);
            this.Controls.Add(this.knightButton);
            this.Controls.Add(this.classLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWin";
            this.Text = "Rollenspiel";
            this.knightBox.ResumeLayout(false);
            this.knightBox.PerformLayout();
            this.archerBox.ResumeLayout(false);
            this.archerBox.PerformLayout();
            this.paladinBox.ResumeLayout(false);
            this.paladinBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label classLabel;
        private System.Windows.Forms.Button knightButton;
        private System.Windows.Forms.Button archerButton;
        private System.Windows.Forms.Button paladinButton;
        private System.Windows.Forms.GroupBox knightBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox archerBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox paladinBox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
    }
}

