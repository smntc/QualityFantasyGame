﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class Potion : AnimationObject
    {
        private Bitmap animation;
        private int health, stamina, mana;

        public Potion(RectangleF bounds, Bitmap image, Bitmap animation, int health, int stamina, int mana)
            : base(bounds, image, 20)
        {
            this.health = health;
            this.stamina = stamina;
            this.mana = mana;

            this.animation = animation;
        }

        public void Drink(Player player)
        {
            player.Health += health;
            player.Stamina += stamina;
            player.Mana += mana;

            StartAnimation(0, player.CalcCenter(), animation, new RectangleF(-25, -25, 50, 50)); 
        }
    }

    public enum PotionType
    {
        Health, Stamina, Mana
    }
}
