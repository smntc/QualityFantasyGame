﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public class Wall : CollisionObject
    {

        public Wall(RectangleF bounds, Bitmap image = null)
            : base(bounds, image)
        {
        }

        protected override void Collide(CollisionObject collider)
        {
            // Ignore
        }
    }
}
