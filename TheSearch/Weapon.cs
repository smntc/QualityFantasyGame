﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace TheSearch
{
    public abstract class Weapon : AnimationObject
    {
        private int damage;

        public int Damage
        {
            get { return damage; }
            set { damage = value; }
        }

        private Bitmap fastAnimation;
        private Bitmap heavyAnimation;

        protected Bitmap FastAnimation
        {
            get { return fastAnimation; }
        }

        protected Bitmap HeavyAnimation
        {
            get { return heavyAnimation; }
        }

        protected Weapon(RectangleF bounds, Bitmap image, Bitmap fastAnimation, Bitmap heavyAnimation, int damage, int animationDuration = 15)
            : base(bounds, image, animationDuration) 
        {
            this.damage = damage;
            this.fastAnimation = fastAnimation;
            this.heavyAnimation = heavyAnimation;
        }

        public abstract void FastAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies);
        public abstract void HeavyAttack(int playerRotation, RectangleF playerBounds, Enemy[] enemies);
    }
}
