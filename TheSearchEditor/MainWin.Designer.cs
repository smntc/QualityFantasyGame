﻿namespace TheSearchEditor
{
    partial class MainWin
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enemyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.batToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ghostToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ghoulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.skeletonToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bossToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wallToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bgCBox = new System.Windows.Forms.ToolStripComboBox();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.koadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.backgroundToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.koadToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1623, 33);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enemyToolStripMenuItem,
            this.wallToolStripMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(58, 29);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // enemyToolStripMenuItem
            // 
            this.enemyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.batToolStripMenuItem,
            this.ghostToolStripMenuItem,
            this.ghoulToolStripMenuItem,
            this.skeletonToolStripMenuItem,
            this.bossToolStripMenuItem});
            this.enemyToolStripMenuItem.Name = "enemyToolStripMenuItem";
            this.enemyToolStripMenuItem.Size = new System.Drawing.Size(137, 30);
            this.enemyToolStripMenuItem.Text = "Enemy";
            // 
            // batToolStripMenuItem
            // 
            this.batToolStripMenuItem.Name = "batToolStripMenuItem";
            this.batToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.batToolStripMenuItem.Text = "Bat";
            this.batToolStripMenuItem.Click += new System.EventHandler(this.AddEnemy);
            // 
            // ghostToolStripMenuItem
            // 
            this.ghostToolStripMenuItem.Name = "ghostToolStripMenuItem";
            this.ghostToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.ghostToolStripMenuItem.Text = "Ghost";
            this.ghostToolStripMenuItem.Click += new System.EventHandler(this.AddEnemy);
            // 
            // ghoulToolStripMenuItem
            // 
            this.ghoulToolStripMenuItem.Name = "ghoulToolStripMenuItem";
            this.ghoulToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.ghoulToolStripMenuItem.Text = "Ghoul";
            this.ghoulToolStripMenuItem.Click += new System.EventHandler(this.AddEnemy);
            // 
            // skeletonToolStripMenuItem
            // 
            this.skeletonToolStripMenuItem.Name = "skeletonToolStripMenuItem";
            this.skeletonToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.skeletonToolStripMenuItem.Text = "Skeleton";
            this.skeletonToolStripMenuItem.Click += new System.EventHandler(this.AddEnemy);
            // 
            // bossToolStripMenuItem
            // 
            this.bossToolStripMenuItem.Name = "bossToolStripMenuItem";
            this.bossToolStripMenuItem.Size = new System.Drawing.Size(152, 30);
            this.bossToolStripMenuItem.Text = "Boss";
            this.bossToolStripMenuItem.Click += new System.EventHandler(this.AddEnemy);
            // 
            // wallToolStripMenuItem
            // 
            this.wallToolStripMenuItem.Name = "wallToolStripMenuItem";
            this.wallToolStripMenuItem.Size = new System.Drawing.Size(137, 30);
            this.wallToolStripMenuItem.Text = "Wall";
            this.wallToolStripMenuItem.Click += new System.EventHandler(this.AddWall);
            // 
            // backgroundToolStripMenuItem
            // 
            this.backgroundToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bgCBox});
            this.backgroundToolStripMenuItem.Name = "backgroundToolStripMenuItem";
            this.backgroundToolStripMenuItem.Size = new System.Drawing.Size(119, 29);
            this.backgroundToolStripMenuItem.Text = "Background";
            // 
            // bgCBox
            // 
            this.bgCBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bgCBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bgCBox.Name = "bgCBox";
            this.bgCBox.Size = new System.Drawing.Size(121, 33);
            this.bgCBox.SelectedIndexChanged += new System.EventHandler(this.OnBgChange);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.OnSaveClick);
            // 
            // koadToolStripMenuItem
            // 
            this.koadToolStripMenuItem.Name = "koadToolStripMenuItem";
            this.koadToolStripMenuItem.Size = new System.Drawing.Size(63, 29);
            this.koadToolStripMenuItem.Text = "Load";
            this.koadToolStripMenuItem.Click += new System.EventHandler(this.OnLoadClick);
            // 
            // MainWin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1623, 867);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWin";
            this.Text = "Form1";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enemyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem batToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ghostToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ghoulToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wallToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox bgCBox;
        private System.Windows.Forms.ToolStripMenuItem koadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem skeletonToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bossToolStripMenuItem;
    }
}

