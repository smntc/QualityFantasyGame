﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TheSearchEditor
{
    public partial class MainWin : Form
    {
        private ContextMenu objContext;
        private PictureBox currObj;

        Dictionary<string, List<PictureBox>> objects;

        public MainWin()
        {
            InitializeComponent();

            Image bg = Image.FromFile("img/map0.png");
            ClientSize = bg.Size;
            BackgroundImage = bg;

            bgCBox.Items.AddRange(Directory.GetFiles("img/", "map*.png"));
            bgCBox.Text = "img/map0.png";

            objContext = new ContextMenu
            (
                new MenuItem[] 
                {
                    new MenuItem("Select", new EventHandler(OnObjectSelect)),
                    new MenuItem("Delete", new EventHandler(OnObjectDelete)),
                    new MenuItem("Move", new EventHandler(OnObjectMove))
                }
            );

            objects = new Dictionary<string,List<PictureBox>>();
            objects.Add("Bat", new List<PictureBox>());
            objects.Add("Ghost", new List<PictureBox>());
            objects.Add("Ghoul", new List<PictureBox>());
            objects.Add("Skeleton", new List<PictureBox>());
            objects.Add("Boss", new List<PictureBox>());
            objects.Add("Wall", new List<PictureBox>());
        }

        private void AddEnemy(object sender, EventArgs args)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;
            Image img = Image.FromFile("img/" + item.Text.ToLower() + ".png");
            PictureBox pb = new PictureBox();
            pb.Image = img;
            pb.Size = img.Size;
            pb.BackColor = Color.Transparent;
            pb.Location = new Point(600, 300);
            pb.ContextMenu = objContext;

            Controls.Add(pb);
            SelectPictureBox(pb);

            objects[item.Text].Add(pb);
        }

        private void AddWall (object sender, EventArgs args)
        {
            using (Settings settings = new Settings("Width", "Height", 25, 100))
            {
                if (settings.ShowDialog() == DialogResult.OK)
                {
                    int width = settings.Paramater1, height = settings.Parameter2;

                    Image img = Image.FromFile("img/wall.png");
                    PictureBox pb = new PictureBox();
                    pb.SizeMode = PictureBoxSizeMode.StretchImage;
                    pb.Image = img;
                    pb.Size = new Size(width, height);
                    pb.BackColor = Color.Transparent;
                    pb.Location = new Point(600, 300);
                    pb.ContextMenu = objContext;

                    Controls.Add(pb);
                    SelectPictureBox(pb);

                    objects["Wall"].Add(pb);
                }
            }
        }

        private void OnMouseDown(object sender, MouseEventArgs args)
        {
            if (args.Button == MouseButtons.Left && currObj != null)
                currObj.Location = new Point(args.X - currObj.Width / 2, args.Y - currObj.Height / 2);
        }

        private void OnObjectSelect(object sender, EventArgs args)
        {
            ContextMenu owner = (ContextMenu)(((MenuItem)sender).Parent);
            SelectPictureBox((PictureBox)owner.SourceControl);      
        }

        private void OnObjectMove(object sender, EventArgs args)
        {
            ContextMenu owner = (ContextMenu)(((MenuItem)sender).Parent);
            PictureBox origin = (PictureBox)owner.SourceControl;
            using (Settings settings = new Settings("X", "Y", origin.Location.X, origin.Location.Y))
            {
                if (settings.ShowDialog() == DialogResult.OK)
                {
                    Point pos = new Point(settings.Paramater1, settings.Parameter2);
                    origin.Location = pos;
                }
            }
        }

        private void SelectPictureBox(PictureBox pb)
        {
            foreach (Control c in Controls)
            {
                if (c.GetType().Equals(typeof(PictureBox)))
                {
                    ((PictureBox)c).BorderStyle = BorderStyle.None;
                }
            }

            currObj = pb;
            currObj.BorderStyle = BorderStyle.FixedSingle;

        }


        private void OnObjectDelete(object sender, EventArgs args)
        {
            ContextMenu owner = (ContextMenu)(((MenuItem)sender).Parent);
            PictureBox origin = (PictureBox)owner.SourceControl;
            Controls.Remove(origin);

            foreach (string key in objects.Keys)
            {
                if (objects[key].Contains(origin))
                {
                    objects[key].Remove(origin);
                }
            }
        }

        private void OnBgChange(object sender, EventArgs args)
        {
            Image img = Image.FromFile(bgCBox.Text);
            BackgroundImage = img;
        }

        private void OnSaveClick(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\rooms\\";
            dialog.Filter = "*.room|*.room";
            dialog.FileName = "room0";
            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            int type = 0;
            using (StreamWriter writer = new StreamWriter(new FileStream(dialog.FileName, FileMode.Create)))
            {
                writer.WriteLine(bgCBox.Text.Split('/')[1]);

                foreach (string key in objects.Keys)
                {
                    if (type == 5)
                        writer.WriteLine('\u001D');

                    foreach (PictureBox pb in objects[key])
                    {
                        if (type < 5)
                        {
                            writer.WriteLine(key.ToLower() + ".png");
                            writer.WriteLine(type);
                            writer.WriteLine(pb.Location.X);
                            writer.WriteLine(pb.Location.Y);
                        }
                        else
                        {
                            writer.WriteLine(key.ToLower() + ".png");
                            writer.WriteLine(pb.Location.X);
                            writer.WriteLine(pb.Location.Y);
                            writer.WriteLine(pb.Width);
                            writer.WriteLine(pb.Height);
                        }
                    }

                    type++;
                }

                writer.WriteLine('\u001D');
            }

            MessageBox.Show("Saving succsessful", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void OnLoadClick(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\rooms\\";
            dialog.Filter = "*.room|*.room";
            dialog.Multiselect = false;

            if (dialog.ShowDialog() != DialogResult.OK)
                return;

            PictureBox[] pbs = Controls.OfType<PictureBox>().ToArray();
            foreach (PictureBox pb in pbs)
                Controls.Remove(pb);

            foreach (string key in objects.Keys)
                objects[key].Clear();

            using (StreamReader reader = new StreamReader(new FileStream(dialog.FileName, FileMode.Open)))
            {
                bgCBox.Text = "img/" + reader.ReadLine();

                while (reader.Peek() != '\u001D')
                {
                    string path = reader.ReadLine();
                    int type = Convert.ToInt32(reader.ReadLine());
                    PictureBox pb = new PictureBox();
                    Image img = Image.FromFile("img/" + path);
                    pb.Image = img;
                    pb.BackColor = Color.Transparent;
                    pb.Size = img.Size;
                    pb.Location = new Point(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));
                    pb.ContextMenu = objContext;

                    Controls.Add(pb);
                    SelectPictureBox(pb);

                    string key;
                    switch (type)
                    {
                        case 0: key = "Bat"; break;
                        case 1: key = "Ghost"; break;
                        case 2: key = "Ghoul"; break;
                        case 3: key = "Skeleton"; break;
                        default: key = "Boss"; break;
                    }
                    objects[key].Add(pb); 
                }
                reader.ReadLine();
                while (reader.Peek() != (int)'\u001D')
                {
                    PictureBox pb = new PictureBox();
                    pb.SizeMode = PictureBoxSizeMode.StretchImage;
                    pb.Image = Image.FromFile("img/" + reader.ReadLine());
                    pb.BackColor = Color.Transparent;
                    pb.Location = new Point(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));
                    pb.Size = new Size(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));
                    pb.ContextMenu = objContext;
                    
                    Controls.Add(pb);
                    SelectPictureBox(pb);

                    objects["Wall"].Add(pb);
                }
            }
        }
    }
}
