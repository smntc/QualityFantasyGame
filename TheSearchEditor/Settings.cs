﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TheSearchEditor
{
    public partial class Settings : Form
    {
        private int parameter1, parameter2;

        public int Paramater1 { get { return parameter1; } }
        public int Parameter2 { get { return parameter2; } }

        public Settings(string parameter1, string parameter2, int value1, int value2)
        {
            InitializeComponent();
            label1.Text = parameter1;
            label2.Text = parameter2;

            textBox1.Text = value1.ToString();
            textBox2.Text = value2.ToString();

            CancelButton = button1;
        }

        private void button2_Click(object sender, EventArgs args)
        {
            try 
            {
                parameter1 = Convert.ToInt32(textBox1.Text);
                parameter2 = Convert.ToInt32(textBox2.Text);
                this.DialogResult = DialogResult.OK;

            }
            catch (FormatException e)
            {
                MessageBox.Show("Invalid parameter\n" + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Cancel;
            }

            this.Close();
        }
    }
}
